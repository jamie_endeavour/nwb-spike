import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';

import Heading from './Heading';

var stories = storiesOf('Heading', module);

// Add the `withKnobs` decorator to add knobs support to your stories.
// You can also configure `withKnobs` as a global decorator.
stories.addDecorator(withKnobs);

stories.add('with static content', function () {
  return React.createElement(
    Heading,
    null,
    'Hello. I am a heading.'
  );
}).add('with editable content', function () {
  var title = text('Title', 'Change Me!');
  return React.createElement(
    Heading,
    null,
    title
  );
});