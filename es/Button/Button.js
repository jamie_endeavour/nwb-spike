import React from 'react';
import BulmaButton from 'react-bulma-components/lib/components/button';

var Button = function Button(_ref) {
  var children = _ref.children,
      _onClick = _ref.onClick;
  return React.createElement(
    BulmaButton,
    {
      onClick: function onClick() {
        return _onClick();
      },
      color: 'primary'
    },
    children
  );
};

export default Button;