import React from 'react'
import { render } from 'react-dom'

import {
  Button,
  Heading,
} from '../../src';

const Demo = () => (
  <div>
    <Heading>Hello this is a heading!</Heading>
    <Button onClick={() => console.log('this has been clicked')}>
      Now Then!
    </Button>
  </div>
);

render(<Demo/>, document.querySelector('#demo'))
