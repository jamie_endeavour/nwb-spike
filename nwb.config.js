module.exports = {
  type: 'react-component',
  npm: {
    esModules: true,
    umd: false
  },
  webpack: {
    extra: {
      resolve: {
        modules: ['node_modules', 'src'],
        // ...
      }
    }
  }
}
