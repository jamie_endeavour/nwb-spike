'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _react3 = require('@storybook/react');

var _addonKnobs = require('@storybook/addon-knobs');

var _Heading = require('./Heading');

var _Heading2 = _interopRequireDefault(_Heading);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var stories = (0, _react3.storiesOf)('Heading', module);

// Add the `withKnobs` decorator to add knobs support to your stories.
// You can also configure `withKnobs` as a global decorator.
stories.addDecorator(_addonKnobs.withKnobs);

stories.add('with static content', function () {
  return _react2.default.createElement(
    _Heading2.default,
    null,
    'Hello. I am a heading.'
  );
}).add('with editable content', function () {
  var title = (0, _addonKnobs.text)('Title', 'Change Me!');
  return _react2.default.createElement(
    _Heading2.default,
    null,
    title
  );
});