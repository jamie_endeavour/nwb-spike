'use strict';

exports.__esModule = true;
exports.Heading = exports.Button = undefined;

var _Button2 = require('./Button');

var _Button3 = _interopRequireDefault(_Button2);

var _Heading2 = require('./Heading');

var _Heading3 = _interopRequireDefault(_Heading2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.Button = _Button3.default; /**
                                    * Using export extensions we can re-export components
                                    * https://github.com/insin/nwb/blob/master/docs/guides/ReactComponents.md#libraries
                                    */

exports.Heading = _Heading3.default;