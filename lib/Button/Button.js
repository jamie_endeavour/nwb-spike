'use strict';

exports.__esModule = true;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _button = require('react-bulma-components/lib/components/button');

var _button2 = _interopRequireDefault(_button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Button = function Button(_ref) {
  var children = _ref.children,
      _onClick = _ref.onClick;
  return _react2.default.createElement(
    _button2.default,
    {
      onClick: function onClick() {
        return _onClick();
      },
      color: 'primary'
    },
    children
  );
};

exports.default = Button;
module.exports = exports['default'];