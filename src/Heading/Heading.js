import React from 'react';

const Heading = ({ children }) => (
  <div>
    <h1>{children}</h1>
  </div>
);

export default Heading;
