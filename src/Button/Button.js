import React from 'react';
import BulmaButton from 'react-bulma-components/lib/components/button';

const Button = ({ children, onClick }) => (
  <BulmaButton
    onClick={() => onClick()}
    color='primary'
  >
    {children}
  </BulmaButton>
);

export default Button;
